       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI-CALCULATOR.
       AUTHOR. KANTINUN.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT         PIC 999V99 VALUE ZEROS.
       01  HEIGHT         PIC 999V99 VALUE ZEROS.
       01  BMI            PIC 99V99  VALUE ZEROS.

       01  RESULT         PIC X(20) VALUE SPACES.
           88 UNDERWEIGHT VALUE "You are Underweight".
           88 NORMAL      VALUE "You are Normal".
           88 OVERWEIGHT  VALUE "You are Overweight".
           88 OBESITY     VALUE "You are Obesity".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter your weight(kg) -: " WITH NO ADVANCING
           ACCEPT WEIGHT
           DISPLAY "Enter your height(cm) -: " WITH NO ADVANCING 
           ACCEPT HEIGHT
           COMPUTE HEIGHT = HEIGHT / 100
           END-COMPUTE 

           COMPUTE BMI = WEIGHT / (HEIGHT ** 2)
           END-COMPUTE 
      *    DISPLAY BMI

           EVALUATE TRUE
              WHEN  BMI < 18.5  SET UNDERWEIGHT TO TRUE
              WHEN  BMI < 25    SET NORMAL TO TRUE
              WHEN  BMI < 30    SET OVERWEIGHT TO TRUE 
              WHEN  OTHER       SET OBESITY TO TRUE
           END-EVALUATE

           PERFORM PRINT-RESULT
           GOBACK
           .
        
       PRINT-RESULT.
           DISPLAY "------------------------------"
           DISPLAY "            RESULT            "
           DISPLAY "------------------------------"
           DISPLAY "Your BMI : " BMI
           DISPLAY RESULT 
           DISPLAY "------------------------------"
           .
